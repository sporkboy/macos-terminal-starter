package pkg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"syscall"
	"time"

	"github.com/prometheus/common/log"
)

// TerminalChecker provides functionality to track the instances of terminals.
type TerminalChecker struct {
	TerminalsFile 	string
	Terminals 		map[int]int
}

// Run does the work
func (t *TerminalChecker) Run() {
	holdThePhone()
	t.getRunningPids()
	t.cleanTerms()
	termID := t.makeTermTitle()
	t.writeTerms()
	t.startBash(termID)
}

// holdThePhone simply waits a few miliseconds and then returns
func holdThePhone() {
	r := rand.Intn(70)
	time.Sleep(time.Duration(r) * time.Microsecond)
}

// getRunningPids checks the .runningTerminals file for pids.
// If .runningTerminals exists and is not empty it sets the content of TerminalChecker.Terminals.
// If it does not exist or is empty it does nothing.
func (t *TerminalChecker) getRunningPids() {
	_, err := os.Stat(t.TerminalsFile)
	if os.IsNotExist(err) {
		return
	}
	terminalsContent, err := ioutil.ReadFile(t.TerminalsFile)
	if err != nil {
		log.Fatalf("Error reading terminals file: %s\n", err.Error())
	}

	if (len(terminalsContent) > 1) {
		var terminals map[int]int

		err = json.Unmarshal(terminalsContent, &terminals)
		if err != nil {
			log.Fatalf("Error unmarshalling terminals: %s", err.Error())
		}

		t.Terminals = terminals
	}

}

// getNextAvailableID returns the next available terminal ID.
// A result of zero indicates an error.
func (t *TerminalChecker) getNextAvailableID() int {
	result := 0
	if len(t.Terminals) == 0 {
		result = 1
	}

	for i := 1; i < len(t.Terminals); i++ {
		if _, ok := t.Terminals[i]; !ok {
			return i
		}
	}
	result = len(t.Terminals) + 1

	log.Debugf("Next available is %d\n", result)
	return result
}

// makeTermTitle sets the terminal title for the current terminal.
func (t *TerminalChecker) makeTermTitle() int {
	termID := t.getNextAvailableID()
	bashPid := os.Getppid()

	t.Terminals[termID] = bashPid

	log.Debugf("Setting terminal")
	//fmt.Printf("export HISTFILE=~/.bash_history_%d", termID)
	fmt.Printf("\033]1;Terminal %d\a", termID)
	return termID
}

// cleanTerms checks the list of terminals to make sure they are actually running and removes any which are not.
func (t *TerminalChecker) cleanTerms() {

	if len(t.Terminals) == 0 {
		return
	}
	for termNum, pid := range t.Terminals {
		process, err := os.FindProcess(pid)
		if err != nil {
			log.Fatalf("Error getting proces: %s", err.Error())
		}

		// send SIGCONT (on Linux) which is a safe signal to the process to test if it exists
		err = process.Signal(syscall.SIGCONT)
		if err != nil {
			log.Debugf("Process %d does not exist: Deleting", pid)
			delete(t.Terminals, termNum)
		} else {
			log.Debugf("Found processes: %d\n", process.Pid)
			log.Debugf("Proc Looks like: %#v\n", process)
		}
	}
}

// writeTerms writes the .runningTerminals
func (t *TerminalChecker) writeTerms() {
	var termsFile *os.File
	_, err := os.Stat(t.TerminalsFile)
	if os.IsNotExist(err) {
		termsFile, err = os.Create(t.TerminalsFile)
		if err != nil {
			log.Fatalf("Error creating terminals file: %s", err.Error())
		}

	} else {
		termsFile, err = os.OpenFile(t.TerminalsFile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
		if err != nil {
			log.Fatalf("Error opening terminals file: %s", err)
		}
	}

	defer termsFile.Close()

	terminalsContent, err := json.Marshal(t.Terminals)
	if err != nil {
		log.Fatalf("Error marshalling terminals list: %s", err.Error())
	}
	_, err = termsFile.Write(terminalsContent)
	if err != nil {
		log.Fatalf("Error writing terminals file: %s", err.Error())
	}
}

// startBash does a syscall.Exec() to a bash login shell.
func (t *TerminalChecker) startBash(termID int) {
	binary, lookErr := exec.LookPath("bash")
	if lookErr != nil {
		panic(lookErr)
	}

	args := []string{"bash", "-l"}

	env := os.Environ()
	histFilePath := fmt.Sprintf("HISTFILE=%s/.bash_history_%d", os.Getenv("HOME") , termID)
	log.Debugf("Setting HISTFILE to [%s]", histFilePath)
	env = append(env, histFilePath)

	execErr := syscall.Exec(binary, args, env)
	if execErr != nil {
		panic(execErr)
	}
}
