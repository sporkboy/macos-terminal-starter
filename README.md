# MacOS Terminal Starter

A small utility which will set your MacOS terminal title to "Terminal #"  
and your HISTFILE to `~/.bash_history_#` where # is the next available number


## Installation

1. `go build -o terminalStarter cmd/main.go`
2. Then move `terminalStarter` to somewhere in your `$PATH`
3. Then in terminal go to Terminal -> Preferences -> Profiles
4. Select the appropriate profile and switch to the 'Shell' tab
5. Select the checkbox next to 'Run command:' and type the path to terminalStarter
6. Deselect "Run inside shell" unless you like exiting twice every time you quit your shell

![Terminal Settings](TerminalSetting.png)
