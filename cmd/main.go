package main

import (
	"gitlab.com/sporkboy/macos-terminal-starter/pkg"
	"os"
)

var (
	terminalsFile = os.Getenv("HOME") + "/.runningTerminals"
)

func main() {
	termChecker := pkg.TerminalChecker{
		TerminalsFile: terminalsFile,
		Terminals: make(map[int]int),
	}

	termChecker.Run()
}
